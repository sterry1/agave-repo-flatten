#!/usr/bin/env bash

echo " flatten apidocs"
./flatten-submodule.sh agave-apidocs 2.1.8
echo " *******  "
echo ""
echo " flatten apps"
./flatten-submodule.sh agave-apps 2.1.8
echo " *******  "
echo ""
echo " flatten auth"
./flatten-submodule.sh agave-auth 2.1.8
echo " *******  "
echo ""
echo " flatten common"
./flatten-submodule.sh agave-common 2.1.8
echo " *******  "
echo ""
echo " flatten files"
./flatten-submodule.sh agave-files 2.1.8
echo " *******  "
echo ""
echo " flatten jobs"
./flatten-submodule.sh agave-jobs 2.1.8
echo " *******  "
echo ""
echo " flatten logging"
./flatten-submodule.sh agave-logging 2.1.8
echo " *******  "
echo ""
echo " flatten metadata"
./flatten-submodule.sh agave-metadata 2.1.8
echo " *******  "
echo ""
echo " flatten migrations"
./flatten-submodule.sh agave-migrations 2.1.8
echo " *******  "
echo ""
echo " flatten monitors"
./flatten-submodule.sh agave-monitors AH-59-include-history-endpoint-in-_links
echo " *******  "
echo ""
echo " flatten notifications"
./flatten-submodule.sh agave-notifications 2.1.8
echo " *******  "
echo ""
echo " flatten postits"
./flatten-submodule.sh agave-postits 2.1.8
echo " *******  "
echo ""
echo " flatten profiles"
./flatten-submodule.sh agave-profiles 2.1.8
echo " *******  "
echo ""
echo " flatten realtime"
./flatten-submodule.sh agave-realtime 2.1.8
echo " *******  "
echo ""
echo " flatten stats"
./flatten-submodule.sh agave-stats 2.1.8
echo " *******  "
echo ""
echo " flatten systems"
./flatten-submodule.sh agave-systems 2.1.8
echo " *******  "
echo ""
echo " flatten tags"
./flatten-submodule.sh agave-tags 2.1.8
echo " *******  "
echo ""
echo " flatten tenants"
./flatten-submodule.sh agave-tenants 2.1.8
echo " *******  "
echo ""
echo " flatten transforms"
./flatten-submodule.sh agave-transforms 2.1.8
echo " *******  "
echo ""
echo " flatten usage"
./flatten-submodule.sh agave-usage 2.1.6
echo " *******  "
echo ""

