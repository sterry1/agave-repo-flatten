#!/usr/bin/env bash
# requires a submodule be passed in for processing
projdir=`pwd`
submod=$1
branch=$2
echo " projdir $projdir"
# start the flattening process in the submodule
# cd into submodule
cd $submod
# rewrite file paths
$projdir/slim-git-rewrite-to-subfolder.sh $submod
sleep 5

# go to the "parent" agave and remove the submodule
cd $projdir/agave
echo "working in `pwd`"

# deinit the submodule
git submodule deinit $submod
# remove the submodule directory
git rm $submod
# commit the removal
git commit -am "removed submodule $submod"

# add local submodule as a remote
git remote add $submod ../$submod

# fetch the local remote submodule
git fetch $submod
# merge the new content into the parent
git merge -s ours --no-commit $submod/$branch

mv $projdir/original/$submod .

rm -rf $submod/.git

git add $submod && git commit -m "integrated $submod"

echo " remove the reference to the local remote $submod"
git remote rm $submod

cd ..