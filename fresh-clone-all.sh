#!/usr/bin/env bash
# develop branch in agave parent module 5-10
# 529f87afaa93bd1a6dbf38dde6da440855d4bfae agave                 (remotes/origin/develop)
# 83856f5a6a4d23f3a9b06c24d61ba5387439990b agave-apidocs         (remotes/origin/2.1.8)
# 613561d1a80b8d7d9de3827853fe31bbde824b89 agave-apps            (heads/2.1.6)
# ff7f230fcc42cf2c192ed189690053b688b30f01 agave-auth            (remotes/origin/2.1.8)
# bef4bea745d580c69feafa243cdcf9aa05140ae0 agave-common          (remotes/origin/2.1.8)
# 58a9e0f369a8344b37e37e3d2ec7ff339609a319 agave-files           (remotes/origin/2.1.8)
# aa503c4b9636bb8f952e9c2e0f02ef641fb9f6d0 agave-jobs            (remotes/origin/2.1.8)
# 3dc10b10bb29dbc136b8db1d33b55b3759aa6fcf agave-logging         (remotes/origin/2.1.8)
# 4eb9ec3039be5eeb89ee13d39b31a86ac472ff35 agave-metadata        (remotes/origin/2.1.8)
# 6e6a1514a8fa0d0eda82768adcda15a240dcb141 agave-migrations      (remotes/origin/2.1.8)
# 3de05f7811cac0a012a8b436d46c11260146535c agave-monitors        (heads/2.1.6)
# 8b120713297db02b01c68887dc764024288e9849 agave-notifications   (remotes/origin/2.1.8)
# 27e9e2770b5133909c9913e5ae30d908ed8028e7 agave-postits         (remotes/origin/2.1.8)
# 9172d395a31557d5c1b3e1fa03d6ac0a4180ea06 agave-profiles        (remotes/origin/2.1.8)
# 31427f483f19952a1b5db70be594a9b713d691fd agave-realtime        (remotes/origin/2.1.8)
# 0bb06ebd5b15b09caa11e304689e6ff85e23550c agave-stats           (remotes/origin/2.1.8)
# d83120e55f3aff6a25c1990a2e5764ce3cb0422b agave-systems         (remotes/origin/2.1.8)
# 1cb58fcc698d977811b0ed7924a885c0222c4730 agave-tags            (remotes/origin/2.1.8)
# 2877fafc19383945c45c89cb2c4772bf013aefa3 agave-tenants         (remotes/origin/2.1.8)
# 3bbfc26abc3ae2936d88089b59a2372d83bcc6fd agave-transforms      (remotes/origin/2.1.8)
# d0b6a1f21175c8a51af679553a968e499221e837 agave-usage           (heads/2.1.6)

# delta from yesterday
# ddfd8b07c485522b4eaf7f991c4f1352f31da022 agave
# 62f47d45ea396bef56cb087c9f5c3738a1ada6be agave-common         (remotes/origin/2.1.8)
# df5b52da9df0dc53a222620736462da44daa7799 agave-files          (remotes/origin/AD-418-files-api-not-honoring-case-insen-1-gdf5b52d)
# 5af875548cc0c96fa0cecaf0c54dbf6848584aaf agave-systems        (remotes/origin/2.1.8)

#  delta for 2.1.8 branch
#  399c55c107231c037283522455315cce4fcb3c4a agave-common        (heads/2.1.6-3-g399c55c)
#  42cb031166898c536b7495a384f778f4b06d7f44 agave-files         (remotes/origin/AD-418-files-api-not-honoring-case-insen)
#  8f586f77cd35046a6dab4b19facb89f13fc803e8 agave-metadata      (remotes/origin/AH-49-vdj-tenant-metadata-queries-are-no)
#  b12934611de5df21c83ebcaf83c3196a5d2e9d96 agave-notifications (heads/2.1.6-1-gb129346)
#  8e403de72b6216abf4d137b026f4018acb77a10a agave-systems       (heads/2.1.6-3-g8e403de)

#  delta for 5-13 from 5-10
# develop branch parent
#+ df4f5875b6be3c5aec10679445beb1c0750f2762 agave                (remotes/origin/develop)
#+ bfbf5234465a5b223936032a702418175b860b32 agave-apidocs        (remotes/origin/2.1.8)
#+ ced114c0bf3e500f8d6a859837250bb992b27ef7 agave-apps           (remotes/origin/2.1.8)
# ff7f230fcc42cf2c192ed189690053b688b30f01 agave-auth           (remotes/origin/2.1.8)
#+ 99d2f76bb9b15fc54841eaea8d97483aba409f69 agave-common         (remotes/origin/2.1.8)
#+ 06bb1b3e8ccb3a76abffa79c05de25f31a840068 agave-files          (remotes/origin/2.1.8)
#+ 58ebb283cd3d3c0a0082c32819923e4a0cbec907 agave-jobs           (remotes/origin/2.1.8)
# 3dc10b10bb29dbc136b8db1d33b55b3759aa6fcf agave-logging        (remotes/origin/2.1.8)
# 4eb9ec3039be5eeb89ee13d39b31a86ac472ff35 agave-metadata       (remotes/origin/2.1.8)
#+ c6d86e292d848923c27b267907b18ba8c15a2e37 agave-migrations     (remotes/origin/2.1.8)
# 3de05f7811cac0a012a8b436d46c11260146535c agave-monitors       (heads/2.1.6)
#+ e0a9abb78b3de8d846760f00df264112e15f3ffc agave-notifications  (remotes/origin/2.1.8)
# 27e9e2770b5133909c9913e5ae30d908ed8028e7 agave-postits        (remotes/origin/2.1.8)
# 9172d395a31557d5c1b3e1fa03d6ac0a4180ea06 agave-profiles       (remotes/origin/2.1.8)
# 31427f483f19952a1b5db70be594a9b713d691fd agave-realtime       (remotes/origin/2.1.8)
# 0bb06ebd5b15b09caa11e304689e6ff85e23550c agave-stats          (remotes/origin/2.1.8)
#+ 12c1c00465b33b94059ca169d5f7b3e12e1363f2 agave-systems        (remotes/origin/2.1.8)
# 1cb58fcc698d977811b0ed7924a885c0222c4730 agave-tags           (remotes/origin/2.1.8)
# 2877fafc19383945c45c89cb2c4772bf013aefa3 agave-tenants        (remotes/origin/2.1.8)
#+ 37711cd491fd6132babd7d624844a7647ff60737 agave-transforms     (remotes/origin/2.1.8)
# d0b6a1f21175c8a51af679553a968e499221e837 agave-usage          (heads/2.1.6)

#  delta for 5-13 to 5-16 freeze day
#  develop branch parent
# d1047b6a5b37953b6955c252b23629b03c652c90 agave                (remotes/origin/develop)
# deca0c342d6a3cd406a9cd2a797f7a2eeea13376 agave-apps           (remotes/origin/2.1.8)
# c127741b8d989e96df8a42642fcd9c0886707b65 agave-files          (remotes/origin/2.1.8)
# b3cc6233bd234f66c0e2c6884855dc11b80bee5a agave-migrations     (remotes/origin/2.1.8)
# 52aef8e92002cc7c99f026d9f34557eead55af27 agave-monitors       (remotes/origin/AH-59-include-history-endpoint-in-_links)
# e31bc7ad8869eb1b2f42ec75116d8e616b56e888 agave-notifications  (remotes/origin/2.1.8)
# 1847ac1bc33c95074a99f7f240dbbcc6baac82b3 agave-systems        (remotes/origin/2.1.8)

#   freeze day 10:15
# d1047b6a5b37953b6955c252b23629b03c652c90 agave                (remotes/origin/develop)
# bfbf5234465a5b223936032a702418175b860b32 agave-apidocs        (remotes/origin/2.1.8)
# deca0c342d6a3cd406a9cd2a797f7a2eeea13376 agave-apps           (remotes/origin/2.1.8)
# ff7f230fcc42cf2c192ed189690053b688b30f01 agave-auth           (remotes/origin/2.1.8)
# 99d2f76bb9b15fc54841eaea8d97483aba409f69 agave-common         (remotes/origin/2.1.8)
# c127741b8d989e96df8a42642fcd9c0886707b65 agave-files          (remotes/origin/2.1.8)
# 58ebb283cd3d3c0a0082c32819923e4a0cbec907 agave-jobs           (remotes/origin/2.1.8)
# 3dc10b10bb29dbc136b8db1d33b55b3759aa6fcf agave-logging        (remotes/origin/2.1.8)
# 4eb9ec3039be5eeb89ee13d39b31a86ac472ff35 agave-metadata       (remotes/origin/2.1.8)
# b3cc6233bd234f66c0e2c6884855dc11b80bee5a agave-migrations     (remotes/origin/2.1.8)
# 52aef8e92002cc7c99f026d9f34557eead55af27 agave-monitors       (remotes/origin/AH-59-include-history-endpoint-in-_links)
# e31bc7ad8869eb1b2f42ec75116d8e616b56e888 agave-notifications  (remotes/origin/2.1.8)
# 27e9e2770b5133909c9913e5ae30d908ed8028e7 agave-postits        (remotes/origin/2.1.8)
# 9172d395a31557d5c1b3e1fa03d6ac0a4180ea06 agave-profiles       (remotes/origin/2.1.8)
# 31427f483f19952a1b5db70be594a9b713d691fd agave-realtime       (remotes/origin/2.1.8)
# 0bb06ebd5b15b09caa11e304689e6ff85e23550c agave-stats          (remotes/origin/2.1.8)
# 1847ac1bc33c95074a99f7f240dbbcc6baac82b3 agave-systems        (remotes/origin/2.1.8)
# 1cb58fcc698d977811b0ed7924a885c0222c4730 agave-tags           (remotes/origin/2.1.8)
# 2877fafc19383945c45c89cb2c4772bf013aefa3 agave-tenants        (remotes/origin/2.1.8)
# 37711cd491fd6132babd7d624844a7647ff60737 agave-transforms     (remotes/origin/2.1.8)
# d0b6a1f21175c8a51af679553a968e499221e837 agave-usage          (heads/2.1.6)

projroot=`pwd`
mkdir original

# init with clean directory
if [ -n "$1" ]
then
   echo " init the layout "
   rm -rf agave*
   rm -rf original/*
fi

echo "****** clone agave ******"
git clone -b develop https://bitbucket.org/agaveapi/agave.git agave && cd agave && git branch -v
cd $projroot
echo ""

echo "****** clone apidocs ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-apidocs.git && cd agave-apidocs && git branch -v
cd $projroot
echo ""

echo "****** clone apps ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-apps.git && cd agave-apps &&  git branch -v
cd $projroot
echo ""

echo "****** clone auth ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-auth.git && cd agave-auth &&  git branch -v
cd $projroot
echo ""

echo "****** clone common ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-common.git && cd agave-common &&  git branch -v
cd $projroot
echo ""

echo "****** clone files ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-files.git agave-files && cd agave-files &&  git branch -v
cd $projroot
echo ""

echo "****** clone jobs ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-jobs.git agave-jobs && cd agave-jobs &&  git branch -v
cd $projroot
echo ""

echo "****** clone logging ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-logging.git agave-logging && cd agave-logging &&  git branch -v
cd $projroot
echo ""

echo "****** clone metadata ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-metadata.git agave-metadata && cd agave-metadata &&  git branch -v
cd $projroot
echo ""

echo "****** clone migrations ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-migrations.git agave-migrations && cd agave-migrations &&  git branch -v
cd $projroot
echo ""

echo "****** clone monitors ******"
git clone -b AH-59-include-history-endpoint-in-_links https://bitbucket.org/agaveapi/agave-monitors.git agave-monitors && cd agave-monitors &&  git branch -v
cd $projroot
echo ""

echo "****** clone notifications ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-notifications.git agave-notifications && cd agave-notifications &&  git branch -v
cd $projroot
echo ""

echo "****** clone postits ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-postits.git agave-postits && cd agave-postits &&  git branch -v
cd $projroot
echo ""

echo "****** clone profiles ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-profiles.git agave-profiles && cd agave-profiles &&  git branch -v
cd $projroot
echo ""

echo "****** clone realtime ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-realtime.git agave-realtime && cd agave-realtime &&  git branch -v
cd $projroot
echo ""

echo "****** clone stats ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-stats.git agave-stats && cd agave-stats &&  git branch -v
cd $projroot
echo ""

echo "****** clone systems ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-systems.git agave-systems && cd agave-systems &&  git branch -v
cd $projroot
echo ""

echo "****** clone tags ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-tags.git agave-tags && cd agave-tags &&  git branch -v
cd $projroot
echo ""

echo "****** clone tenants ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-tenants.git agave-tenants && cd agave-tenants &&  git branch -v
cd $projroot
echo ""

echo "****** clone transforms ******"
git clone -b 2.1.8 https://bitbucket.org/agaveapi/agave-transforms.git agave-transforms && cd agave-transforms &&  git branch -v
cd $projroot
echo ""

echo "****** clone usage ******"
git clone -b 2.1.6 https://bitbucket.org/agaveapi/agave-usage.git agave-usage && cd agave-usage &&  git branch -v
cd $projroot
echo ""

# copy all the submodules to an original local for adding content
cp -R $projroot/agave-* original
