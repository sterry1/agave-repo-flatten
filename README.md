Steve Terry [12:50 PM]
I would like everyone to pull this repo  https://bitbucket.org/sterry1/agave-repo-flatten.git and run these two commands

./fresh-clone-all.sh init
wait for it to complete then run

./flatten-all.sh 
this should leave an agave repository integrated with the submodules
“DON’T PUSH” any of these back to bitbucket

and let me know what you see, code wise and log wise.<br/>
since this is repo for working on repos don’t issue any git commands at the project root to limit confusion.

This process leaves the Agave parent repository integrated with all the submodules and retains the history of commits for each submodule. The develop branch is the leading branch for used for this flattening, however other branches such as 2.1.6 and 2.1.8, if in existence in a submodule, may have commit history not found in develop if the branches were not merged in the submodule. They will most certainly be left with submodule configuration files and remote references not found in develop. We must decide how to structure release and workflow going forward to minimize confusion and error.