#!/bin/bash
# We need the TAB character for SED (Mac OS X sed does not understand \t)
TAB="$(printf '\t')"

TARGET_PATH=$(echo -n "$1" | sed -e 's/[\/&]/\\&/g')
echo "TARGET_PATH : $TARGET_PATH"
git ls-files -s | sed "s/${TAB}/${TAB}$TARGET_PATH\//"
echo ""
echo " start processing "
# The actual processing happens here
CMD="git ls-files -s | sed \"s/${TAB}/${TAB}$TARGET_PATH\//\" | GIT_INDEX_FILE=\${GIT_INDEX_FILE}.new git update-index --index-info && mv \${GIT_INDEX_FILE}.new \${GIT_INDEX_FILE}"

git filter-branch \
    --index-filter "$CMD" \
    HEAD
